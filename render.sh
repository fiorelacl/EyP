#!/bin/bash -x

cd $(dirname $0)

### copy everything to a tmp folder
rm -rf tmp/
rsync -a . tmp/
cd tmp/

### render all the formats
rm -rf ../public/
Rscript -e "bookdown::render_book('index.Rmd', 'all', output_dir = '../public')"

### copy font Monaco
cd ..
cp data/Monaco.woff public/

### clean up
rm -rf tmp/
